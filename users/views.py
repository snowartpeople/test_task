from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.views.generic import FormView, View


from django.contrib.auth.decorators import login_required

# Create your views here.
class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "users/login.html"
    success_url = '/in'

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/login/")

@login_required
def main(request):
    return render(request, 'users/main.html', {})

