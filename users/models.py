from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    MALE = "M"
    FEMALE = "F"

    SEX_CHOICES = (
        (MALE, "Male"),
        (FEMALE, "Female"),
    )

    sex = models.CharField(max_length=1, choices=SEX_CHOICES, default=MALE)