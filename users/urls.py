from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView


from .views import LogoutView, LoginFormView, main


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),


    # authentication
    url(r'^login/$', LoginFormView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^in$', main, name='main'),
    url(r'^$', RedirectView.as_view(pattern_name='login', permanent=False)),

]